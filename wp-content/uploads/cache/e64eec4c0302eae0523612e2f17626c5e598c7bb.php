  <!doctype html>
<html <?php echo get_language_attributes(); ?>>
  <?php echo $__env->make('partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php 
  $quer_object = get_queried_object();
  $bgImage = "";
  if(isset($quer_object->name) && $quer_object->name == 'pakalpojumi'){
    $bgImage = "bg-image";
  }
  
  ?>
  <body <?php body_class( $bgImage ) ?>>
    <?php do_action('get_header') ?>
    <?php if( !isset($_COOKIE['set_lang'])){ ?>
        <?php echo $__env->make('partials.language', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php } ?>
    
    <?php echo $__env->make('partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="wrapper">

        <div class="main">
          <?php echo $__env->yieldContent('content'); ?>
          <?php echo $__env->make('partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </main>
      </div>
    <?php do_action('get_footer') ?>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/rellax.min.js"></script>
     <script async data-main="<?php echo get_template_directory_uri(); ?>/assets/js/app.js?v=1.1.0"
                src="<?php echo get_template_directory_uri(); ?>/assets/js/require.min.js"></script>
    <?php wp_footer() ?>
    <?php echo $__env->make('partials.photoswipe', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</html>
