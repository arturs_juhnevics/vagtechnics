<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php
$description = rwmb_meta( 'equip_archive_text', array( 'object_type' => 'setting'), 'settings');
?>
<div class="container">
	<div class="archive-desc animate animate__fade">
		<p><?php echo e($description); ?></p>
	</div>
</div>

<?php
	$query = new WP_Query( array(
	    'post_type' => 'iekartas',
	    'post_status' => 'publish',
	    'posts_per_page' => -1,
	) );

	$current_page = get_queried_object();
?>

<div class="container equipment">

	<?php while($query->have_posts()): ?> <?php $query->the_post() ?>
		<?php echo $__env->make('partials.equip-item', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php endwhile; ?>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>