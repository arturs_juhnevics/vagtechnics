<?php 
	$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
	$logo = reset( $logos );
	$header_image = $logo['full_url'];

	$lang_args = array(
		'raw'=>1
	);

?>
<div class="language-page">
	<div class="overlay overlay--solid"></div>
	<div class="content">
		<div class="lang-logo">
			<img src="<?php echo e($header_image); ?>" alt="vagtechnics">
		</div>
		<div class="lang-chooser">
			<?php foreach( pll_the_languages($lang_args) as $lang ) : ?>

				<?php 
					$name = "";
					if( $lang['name'] == 'LV'){
						$name = pll__("Latvian", 'language');
						$flag = get_template_directory_uri()."/assets/images/latvia.png";
					}elseif( $lang['name'] == 'EN' ){
						$flag = get_template_directory_uri()."/assets/images/united-states.png";
						$name = pll__("English", 'language');
					}elseif( $lang['name'] == 'RU' ){
						$flag = get_template_directory_uri()."/assets/images/russia.png";
						$name = pll__("Russian", 'language');
					}elseif( $lang['name'] == 'SV' ){
						$flag = get_template_directory_uri()."/assets/images/sweden.png";
						$name = pll__("Swedish", 'language');
					}
				?>
	  	
	  				<a class="lang-chooser__item" lang="<?php echo $lang['slug']; ?>" hreflang="<?php echo $lang['slug']; ?>" href="<?php echo $lang['url']; ?>"><img alt="<?php echo $name; ?>" src="<?php echo $flag; ?>"><span><?php echo $name; ?></span><span class="arrow"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span></a>
	  			
	  		<?php endforeach; ?>
		</div>
		
	</div>
</div>