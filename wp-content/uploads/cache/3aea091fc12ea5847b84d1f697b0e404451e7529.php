<?php $__env->startSection('content'); ?>

<?php 
$enable_aboutus = rwmb_meta('enable_aboutus'); 

?>

    <?php echo $__env->make('partials.home.hero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.home.services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.home.industry', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if( $enable_aboutus ) : ?>
    	<?php echo $__env->make('partials.home.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>

    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>