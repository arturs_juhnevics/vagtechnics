<?php
$slides = get_posts( array(
    'post_type' => 'slider',
    'numberposts' => -1,
    'post_status' => 'publish',
) );
?>
<div class="hero-wrapper">
	<div class="hero">
	<?php $__currentLoopData = $slides; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php
		$image = get_the_post_thumbnail_url($slide->ID);

		$title = get_post_meta($slide->ID, 'slider_title');
		$text = get_post_meta($slide->ID, 'slider_text');
		$buttonText = get_post_meta($slide->ID, 'button_text');
		$buttonUrl = get_post_meta($slide->ID, 'button_url');
		$icons = rwmb_meta('hero_icon_image', array( 'size' => 'medium' ), $slide->ID);

		?>
		<div class="hero__item animate full-width">
			<div class="overlay"></div>
			<img class="hero__bg" data-lazy="<?php echo e($image); ?>"/>
			<div class="container hero__content ">
	            <div class="hero__item__inner">
	            	<h2 class="hero__title"><?php echo e($title[0]); ?></h2>
	            		<?php if($text): ?>
	                    	<div class="hero__text"><?php echo e($text[0]); ?></div>
	                    <?php endif; ?>
	                    <?php if($buttonUrl): ?>
						<a href="<?php echo e($buttonUrl[0]); ?>" class="button"><?php echo e($buttonText[0]); ?></a> 
						<?php endif; ?>
	           	</div>
	        </div>
	        <?php if( isset($icons) ) : ?>
				<?php foreach ($icons as $item) : ?>
	                <?php 
	                $icon = reset($item); 
	                ?>
	                <div class="hero__icons">
	                	<div class="container">
	                		<img src="<?php echo e($icon['url']); ?>" />
	                	</div>
	                </div>
	          	<?php endforeach; ?>
	      	<?php endif; ?>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
	<div class="slider-dots container mob-hidden "></div>
	
</div>
