<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.page-header-simple', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
$description = rwmb_meta( 'services_archive_text', array( 'object_type' => 'setting'), 'settings');
?>
<div class="container">
	<div class="archive-desc animate animate__fade">
		<p><?php echo e($description); ?></p>
	</div>
</div>

<div class="container products spec-list">
	<div class="row">
		<?php
		$query = new WP_Query( array(
		    'post_type' => 'pakalpojumi',
		    'post_status' => 'publish',
		    'numberposts' => -1,
		    'post_parent' => 0,
		) );
		 ?>
  		<?php while($query->have_posts()): ?> <?php $query->the_post() ?>
			<?php 
				$id = get_the_ID();
				$image = get_the_post_thumbnail_url($id, 'medium');
				$url = get_the_permalink();
				$short_desc = rwmb_meta('short_description');
				$desc_croped = tokenTruncate($short_desc, 150) . " ...";
			?>
			
			<div class="col-sm-4">
				<a href="<?php echo e($url); ?>">
			
				<div class="posts__item animate animate__fade">
					<div class="posts__item__image animate animate__fade" style="background-image: url(<?php echo e($image); ?>)">
					</div>
					<div class="posts__item__content animate animate__fade">
						<h3 class="posts__item__content__title"><?php echo get_the_title(); ?></h3>
						<p class="posts__item__content__text"><?php echo $desc_croped; ?></p>
						<a class="button--read-more animate" href="<?php echo e($url); ?>"><?php echo pll__('Lasīt vairāk', 'General'); ?> <span class="arrow"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span></a>
					</div>
				</div>
				</a>
			</div>
			
		<?php endwhile; ?>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>