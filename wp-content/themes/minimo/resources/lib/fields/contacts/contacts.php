<?php
add_filter( 'rwmb_meta_boxes', 'contacts_meta_boxes' );
function contacts_meta_boxes( $meta_boxes ) {

    $meta_boxes[] = array(
        'title'      => 'Company details',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'pvn_no',
                'name' => 'PVN number',
                'type' => 'text',
            ),
            array(
                'id'   => 'legal_address',
                'name' => 'Legal address',
                'type' => 'text',
            ),
            array(
                'id'   => 'actual_address',
                'name' => 'Actual address',
                'type' => 'text',
            ),
            array(
                'id'   => 'lum_bank',
                'name' => 'Luminor bank account',
                'type' => 'text',
            ),
            array(
                'id'   => 'lum_bank_code',
                'name' => 'Luminor bank code',
                'type' => 'text',
            ),
            array(
                'id'   => 'swed_bank',
                'name' => 'Swedbank bank account',
                'type' => 'text',
            ),
            array(
                'id'   => 'swed_bank_code',
                'name' => 'Swedbank bank code',
                'type' => 'text',
            ),
        ),

    );
     $meta_boxes[] = array(
        'title'      => 'Team',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'        => 'enable_team',
                'name'      => 'Enable team',
                'type'      => 'switch',
                
                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),
            array(
                'name'   => 'Team member', // Optional
                'id'     => 'team_members',
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => array( 'field' => 'name' ),
                'clone'  => true,
                'fields' => array(
                    array(
                        'name' => 'Position',
                        'id'   => 'position',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Name',
                        'id'   => 'name',
                        'type' => 'text',
                    ),
                    array(
                        'name' => 'Phone',
                        'id'   => 'phone',
                        'type' => 'text',
                        'clone' => true,
                        'add_button' => 'Add another phone',
                    ),
                    array(
                        'name' => 'Email',
                        'id'   => 'email',
                        'type' => 'text',
                    ),
                ),
            ),   

        )
    );
    $meta_boxes[] = array(
        'title'      => 'Contact form',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-contacts.blade.php')
        ),
        'name' =>'map',
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'contact_text',
                'name' => 'Text',
                'type' => 'wysiwyg',
                'size' => '50',
            ),
             array(
                'id'   => 'contact_address',
                'name' => 'Address',
                'type' => 'text',
                'size' => '50',
            ),
            array(
                'id'            => 'contact_map',
                'name'          => 'Location',
                'type'          => 'map',
                'address_field' => 'contact_address',
                'api_key'       => 'AIzaSyBaob1XUk6NQ7RmmsB4QuhqjGh3foYE3EE',
            ),
            array(
                'name' => 'Pin',
                'id'   => 'contact_map_pin',
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'max_status' => false,
            ),
            array(
                'id'   => 'contact_email',
                'name' => 'Contact Email',
                'type' => 'text',
                'size' => '50',
            ),
        )
    );

    return $meta_boxes;
}