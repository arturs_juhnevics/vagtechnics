<?php
add_filter( 'rwmb_meta_boxes', 'home_meta_boxes' );
function home_meta_boxes( $meta_boxes ) {
     $meta_boxes[] = array(
        'title'      => 'About us',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-home.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'aboutus_heading',
                'name' => 'Heading',
                'type' => 'text',
               
            ),
            array(
                'id'   => 'about_text',
                'name' => 'Text',
                'type' => 'textarea',
            ),
             array(
                'id'   => 'about_button',
                'name' => 'Button title',
                'type' => 'text',
            ),
             array(
                'id'   => 'about_url',
                'name' => 'URL',
                'type' => 'text',
            ),
            array(
                'id'               => 'about_image',
                'name'             => 'Image',
                'type'             => 'image_advanced',

                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,

                // Maximum image uploads.
                'max_file_uploads' => 1,

                // Do not show how many images uploaded/remaining.
                'max_status'       => 'false',

                // Image size that displays in the edit page.
                'image_size'       => 'thumbnail',
            ),
            array(
                'id'               => 'icon_image',
                'name'             => 'Icon',
                'type'             => 'image_advanced',
                'clone'            => true,

                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,

                // Maximum image uploads.
                'max_file_uploads' => 1,

                // Do not show how many images uploaded/remaining.
                'max_status'       => 'false',

                // Image size that displays in the edit page.
                'image_size'       => 'thumbnail',
            ),
            array(
                'id'        => 'enable_aboutus',
                'name'      => 'Enable about us',
                'type'      => 'switch',
                
                // Style: rounded (default) or square
                'style'     => 'rounded',

                // On label: can be any HTML
                'on_label'  => 'Yes',

                // Off label
                'off_label' => 'No',
            ),
           
        ),
        
    );
    $meta_boxes[] = array(
        'title'      => 'Industry',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-home.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'ind_heading',
                'name' => 'Heading',
                'type' => 'text',
               
            ),
             array(
                'id'   => 'ind_text',
                'name' => 'Text',
                'type' => 'textarea',
            ),
             array(
                'name'   => 'Industry item',
                'id'     => 'ind_item',
                'type'   => 'group',
                'collapsible' => true,
                'group_title' => array( 'field' => 'name' ),
                'clone'  => true,
                'fields' => array(
                    array(
                        'name' => 'Name',
                        'id'   => 'ind_name',
                        'type' => 'text',
                    ),
                    array(
                        'id'               => 'ind_image',
                        'name'             => 'Image',
                        'type'             => 'image_advanced',

                        // Delete image from Media Library when remove it from post meta?
                        // Note: it might affect other posts if you use same image for multiple posts
                        'force_delete'     => false,

                        // Maximum image uploads.
                        'max_file_uploads' => 1,

                        // Do not show how many images uploaded/remaining.
                        'max_status'       => 'false',

                        // Image size that displays in the edit page.
                        'image_size'       => 'thumbnail',
                    ),
                    
                ),
            ),
             
        ),      
    );
    return $meta_boxes;
}