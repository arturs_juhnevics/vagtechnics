<?php
add_filter( 'rwmb_meta_boxes', 'about_meta_boxes' );
function about_meta_boxes( $meta_boxes ) {

    $meta_boxes[] = array(
        'title'      => 'Side menu',
        'post_types' => array('page'),
        'include' => array(
            'relation'        => 'OR',
            'template'        => array('views/template-about.blade.php')
        ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
           array(
                'id'               => 'about_icon_image',
                'name'             => 'Icon',
                'type'             => 'image_advanced',
                'clone'            => true,

                // Delete image from Media Library when remove it from post meta?
                // Note: it might affect other posts if you use same image for multiple posts
                'force_delete'     => false,

                // Maximum image uploads.
                'max_file_uploads' => 1,

                // Do not show how many images uploaded/remaining.
                'max_status'       => 'false',

                // Image size that displays in the edit page.
                'image_size'       => 'thumbnail',
            ),
        ),

    );

    return $meta_boxes;
}