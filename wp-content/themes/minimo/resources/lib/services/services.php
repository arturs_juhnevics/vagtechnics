    <?php

class Service{

    public function __construct() {

        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'template-pakalpojumi.php'
        ));
        $page = reset($pages);
        $pageName = isset($page->post_name) ? $page->post_name : 'pakalpojumi';


        $labels = array(
            'name'               => pll__('Pakalpojumi', 'General'),
            'singular_name'      => "Service",
            'menu_name'          => "Services",
            'add_new_item'       => "Add service",
            'new_item'           => "New service",
            'edit_item'          => "Edit service",
            'view_item'          => "View service",
            'all_items'          => "All service",
            'search_items'       => "Search Products",
            'not_found'          => "No Product found",
            'not_found_in_trash' => "No Product found in trash",
            'parent_item'        => "Parent category",
            "parent_item_colon"  => "Parent category:"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'page',
            'map_meta_cap' => true,
            'hierarchical' => true,
            'has_archive' => true,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => true,
            'supports' => array('thumbnail', 'editor', 'title', 'page-attributes'),    
        );

        register_post_type('pakalpojumi', $args);

        add_filter( 'rwmb_meta_boxes', 'product_single_meta_boxes' );

        function product_single_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'product',
                'title'      => 'Product',
                'post_types' => 'pakalpojumi',  
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'id'               => 'short_description',
                        'name'             => 'Short description',
                        'type'             => 'textarea',
                    ),       
                    
                )
            );

            return $meta_boxes;
        }

    }

}
new Service();