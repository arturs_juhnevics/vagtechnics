<?php
function widgets_area_init() {

	register_sidebar( array(
		'name'          => 'Product filter',
		'id'            => 'product_filter',
		'before_widget' => '<div class="product-filter">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="product-filter__title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'widgets_area_init' );

function tokenTruncate($string, $your_desired_width) {
  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
  $parts_count = count($parts);

  $length = 0;
  $last_part = 0;
  for (; $last_part < $parts_count; ++$last_part) {
    $length += strlen($parts[$last_part]);
    if ($length > $your_desired_width) { break; }
  }

  return implode(array_slice($parts, 0, $last_part));
}
