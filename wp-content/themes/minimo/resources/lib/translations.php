<?php

$strings = array(
    'Go to homepage' => 'General',
    'Pakalpojumi' => 'General',
    'Skatīt visus pakalpojumus' => 'General',
    'Iekārtas' => 'General',
    'Lasīt vairāk' => 'General',

    'Lasīt vairāk' => 'General2',

    'Kontaktinformācija' => 'Contacts',
    'Biroja adrese' => 'Contacts',
    'Darbalaiks' => 'Contacts',
    'E-pasts' => 'Contacts',
    'Telefons' => 'Contacts',
    'Rekvizīti' => 'Contacts',
    'Juridiskā adrese' => 'Contacts',
    'PVN reģistrācijas numurs' => 'Contacts',
    'Faktiskā adrese' => 'Contacts',
    'Bankas:' => 'Contacts',
    'Luminor bankas konts' => 'Contacts',
    'Swedbankas konts' => 'Contacts',
    'Komanda' => 'Contacts',
    'Sazinies ar mums' => 'Contacts',

    'Vārds' => 'Contact-form',
    'Telefons' => 'Contact-form',
    'Epasts' => 'Contact-form',
    'Ziņa' => 'Contact-form',
    'Nosūtīt' => 'Contact-form',
    'Ievelc failus šeit vai spied lai augšuplādētu' => 'Contact-form',

    'Sākums' => 'General',

    "Latvian" => 'language',
    "English" => 'language',
    "Russian" => 'language',
    "Swedish" => 'language',

    'Kontakti' => 'footer',
    'Sociālie tīkli' => 'footer',


);

if( function_exists('pll_register_string') ) {
    foreach ( $strings as $string => $category ) {
        pll_register_string($string, $string, $category);
    }
}
