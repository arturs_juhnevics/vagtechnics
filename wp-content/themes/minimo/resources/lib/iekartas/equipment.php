    <?php

class Equipment{

    public function __construct() {

        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'template-iekartas.php'
        ));
        $page = reset($pages);
        $pageName = isset($page->post_name) ? $page->post_name : 'pakalpojumi';


        $labels = array(
            'name'               => pll__('Iekārtas', 'General'),
            'singular_name'      => "Equipment",
            'menu_name'          => "Equipment",
            'add_new_item'       => "Add Equipment",
            'new_item'           => "New Equipment",
            'edit_item'          => "Edit Equipment",
            'view_item'          => "View Equipment",
            'all_items'          => "All",
            'search_items'       => "Search equipment",
            'not_found'          => "No Equipment found",
            'not_found_in_trash' => "No Equipment found in trash",
            'parent_item'        => "Parent category",
            "parent_item_colon"  => "Parent category:"
        );


        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'capability_type' => 'page',
            'map_meta_cap' => true,
            'hierarchical' => false,
            'has_archive' => false,
            'menu_position' => 30,
            'menu_icon' => 'dashicons-portfolio',
            'show_in_rest' => false,
            'supports' => array('thumbnail', 'title'),    
        );

        register_post_type('iekartas', $args);

        add_filter( 'rwmb_meta_boxes', 'equipment_single_meta_boxes' );

        function equipment_single_meta_boxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'id'         => 'general',
                'title'      => 'Specification',
                'post_types' => 'iekartas',  
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(
                    array(
                        'name' => 'Sub title',
                        'id'   => 'sub_title',
                        'type' => 'text',
                    ),
                )
            );
            $meta_boxes[] = array(
                'id'         => 'equipment',
                'title'      => 'Specification',
                'post_types' => 'iekartas',  
                'context'    => 'normal',
                'priority'   => 'high',
                'fields' => array(

                    array(
                        'name'   => 'Values', // Optional
                        'id'     => 'specification',
                        'type'   => 'group',
                        'clone'  => true,
                        'fields' => array(
                            array(
                                'name' => 'Title',
                                'id'   => 'eq_title',
                                'type' => 'text',
                            ),
                            array(
                                'name' => 'Value',
                                'id'   => 'eq_value',
                                'type' => 'text',
                            ),
                        ),
                    ),      
                )
            );

            return $meta_boxes;
        }

    }

}
new Equipment();