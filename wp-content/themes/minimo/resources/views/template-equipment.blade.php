{{--
  Template Name: Equipment
--}}
@extends('layouts.app')

@section('content')
@include('layouts.page-header')

<?php
$description = rwmb_meta( 'equip_archive_text', array( 'object_type' => 'setting'), 'settings');
?>
<div class="container">
	<div class="archive-desc animate animate__fade">
		<p>{{ $description }}</p>
	</div>
</div>

<?php
	$query = new WP_Query( array(
	    'post_type' => 'iekartas',
	    'post_status' => 'publish',
	    'posts_per_page' => -1,
	) );

	$current_page = get_queried_object();
?>

<div class="container equipment">

	@while($query->have_posts()) @php $query->the_post() @endphp
		@include('partials.equip-item')
	@endwhile

</div>

@endsection