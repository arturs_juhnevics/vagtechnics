{{--
  Template Name: Home
--}}
@extends('layouts.app')
@section('content')

<?php 
$enable_aboutus = rwmb_meta('enable_aboutus'); 

?>

    @include('partials.home.hero')
    @include('partials.home.services')
    @include('partials.home.industry')
    <?php if( $enable_aboutus ) : ?>
    	@include('partials.home.about')
    <?php endif; ?>

    
@endsection