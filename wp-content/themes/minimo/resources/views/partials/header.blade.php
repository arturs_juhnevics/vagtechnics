@php
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];
$lang_args = array(
	'raw'=>1
);
$estore_url = rwmb_meta( 'e_store_url', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

$quer_object = get_queried_object();
$lightHeader = "light-header";
 if(isset($quer_object->name) && $quer_object->name == 'pakalpojumi'){
	$lightHeader = "";
}
@endphp
<header class="">
<nav class="navigation {{ $lightHeader }}">
	<div class="main-nav-container">
		<div class="container">
			<div class="menu-overlay"></div>
			<a class="navbar-brand" href="/"><img src="{{ $header_image }}" alt="vagtechnics"></a> 
			<div class="menu animate">
				@if (has_nav_menu('primary_navigation'))
		    		{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
		  		@endif
		  		
		  	</div>
		  	<div class="lang_menu">
		  		<ul class="dropdown">
			  		<?php foreach( pll_the_languages($lang_args) as $lang ) : ?>
			  			<?php if( $lang['current_lang'] == true ) : ?>
			  				<button class="dropdown-toggle lang-button" type="button" id="dropdownLang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $lang['name']; ?></button>
			  			<?php endif; ?>
			  		<?php endforeach; ?>
			  		<div class="dropdown-menu" aria-labelledby="dropdownLang">
				  		<?php foreach( pll_the_languages($lang_args) as $lang ) : ?>
				  			<?php $current = ''; ?>
				  			<?php if( $lang['current_lang'] == true ) :  ?>
				  				<?php $current = 'current' ?>
				  			<?php endif; ?>
				  				<li class="lang-item dropdown-item <?php echo $current; ?>"><a lang="<?php echo $lang['slug']; ?>" hreflang="<?php echo $lang['slug']; ?>" href="<?php echo $lang['url']; ?>"><?php echo $lang['name']; ?></a></li>
				  			
				  		<?php endforeach; ?>
			  		</div>
			  	</ul>
			</div>
		  	<button class="hamburger hamburger--squeeze" type="button">
			  	<span class="hamburger-box">
			    	<span class="hamburger-inner"></span>
			  	</span>
			</button>
		</div>

	</div>
</nav>
</header>