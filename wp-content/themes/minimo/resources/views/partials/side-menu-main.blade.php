<?php
	$current_page = get_queried_object();
	$query = new WP_Query( array(
	    'post_type' => 'pakalpojumi',
	    'post_status' => 'publish',
	    'numberposts' => -1,
	    'post_parent' => 0,
	    'post__not_in' => array($current_page->ID),
	) );


?>
<div class="side-menu">

	@while($query->have_posts()) @php $query->the_post() @endphp
			<?php 
				$id = get_the_ID();
				$image = get_the_post_thumbnail_url($id, 'medium');
				$url = get_the_permalink();
				$short_desc = rwmb_meta('short_description');
				$desc_croped = tokenTruncate($short_desc, 150) . " ...";
			?>
				<a href="{{ $url }}">
			
				<div class="posts__item animate animate__fade">
					<div class="posts__item__image animate animate__fade" style="background-image: url({{ $image }})">
					</div>
					<div class="posts__item__content animate animate__fade">
						<h3 class="posts__item__content__title"><?php echo get_the_title(); ?></h3>
						<a class="button--read-more animate" href="{{ $url }}"><?php echo pll__('Lasīt vairāk', 'General2'); ?> <span class="arrow"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span></a>
					</div>
				</div>
				</a>

			
		@endwhile
</div>