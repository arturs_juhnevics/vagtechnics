<?php
	$query = new WP_Query( array(
	    'post_type' => 'pakalpojumi',
	    'post_status' => 'publish',
	    'numberposts' => -1,
	    'post_parent' => 0,
	) );

	$current_page = get_queried_object();
?>
<div class="side-menu">

	<div class="accordion" id="side-menu">
		<?php $group = 0; ?>
	 	@while($query->have_posts()) @php $query->the_post() @endphp
	 		<?php 
	 			$group_name = 'group-' . $group;
	 			$title = get_the_title(); 
				$url = get_the_permalink();
				$id = get_the_ID();

				$activeMain = false;
				$activeClass = 'active';
				if( $current_page->ID == $id || $current_page->post_parent == $id) :
					$activeMain = true;
				endif;

				$children = get_children(
					array(
						'post_type' => 'pakalpojumi',
						'post_parent' => $id
					)
				);
	 		?>
		  <div class="side-menu__group">
		    <div class="side-menu__group__header" id="headingOne">
		        <a class="side-menu__group__link side-menu__group__link--large <?php echo ($activeMain) ? $activeClass : ""; ?>" href="{{ $url }}" >{{ $title }}</a>
		        <?php if ( !empty($children) ) : ?>
		        <span data-toggle="collapse" data-target="#{{ $group_name }}" aria-expanded="true" aria-controls="{{ $group_name }}"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/chevron-down.svg"); ?></span>
		        <?php endif; ?>
		    </div>

		    <div id="{{ $group_name }}" class="collapse show side-menu__group__children" data-parent="#side-menu">
		        <?php
				
				
				?>
				<?php foreach ($children as $item) : ?>
				<?php 
					$activeSub = false;
					if(  $item->ID == $current_page->ID ) :
						$activeSub = true;
					endif;
				?>
					<a class="side-menu__group__link <?php echo ($activeSub) ? $activeClass : ""; ?>" href="{{ get_the_permalink( $item->ID ) }}">{{ $item->post_title }}</a>
				<?php endforeach; ?>
		    </div>
		  </div>
		  <?php $group++; ?>
  		@endwhile
	</div>

</div>