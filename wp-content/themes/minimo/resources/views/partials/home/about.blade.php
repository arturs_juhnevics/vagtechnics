
<?php 
  $current_page = get_queried_object();
	$heading = rwmb_meta('aboutus_heading', false, $current_page->ID);

	$text = rwmb_meta('about_text', false, $current_page->ID);
	$button_text = rwmb_meta('about_button', false, $current_page->ID);
	$button_url = rwmb_meta('about_url', false, $current_page->ID);
	$images = rwmb_meta( 'about_image', array( 'size' => 'large' ), $current_page->ID );
  $image = reset($images);
  
  $icons = rwmb_meta('icon_image', array( 'size' => 'medium' ), $current_page->ID);
?>

<div class="section">
<div class="full-width-block">
          

      <div class="container">
        <div class="row">

          <div class="col-sm-6">
            <div class="about__content animate animate__fade">
              <h2 class="about__heading"><?php echo $heading; ?></h2>
              <p class="about__text"><?php echo $text; ?></p>
              <a class="button--read-more animate animate__fade-up" href="{{ $button_url }}">{{ $button_text }} <span class="arrow"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span></a>

              <?php foreach ($icons as $item) : ?>

                <?php 
                $icon = reset($item); 
                ?>
                <div class="about__icons">
                  <img src="{{ $icon['url'] }}" />
                </div>
              <?php endforeach; ?>
            </div>
           	
          </div>

          <div class="col-sm-6">
            
          </div>

        </div>
      </div><!-- END container -->

      <div class="full-width-block__left">
        
      </div>
      <div class="full-width-block__right animate animate__fade">
        <div class="overlay overlay--solid"></div>
        <div class="about__image" style="background-image: url('<?php echo $image['url']; ?>')"></div>
      </div><!-- END full-width-block__right -->

  </div><!-- END full-width-block -->
</div>