<?php 
	$heading = rwmb_meta('cash_title');
	$text = rwmb_meta('cash_text');
	$button_text = rwmb_meta('cash_button');
	$button_url = rwmb_meta('cash_url');
	$images = rwmb_meta( 'cash_image', array( 'size' => 'large' ) );
	$image = reset($images);

	$hours = rwmb_meta('working_hours', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
?>
<div class="container section section--text-image content-right">
	<div class="row">

		<div class="col-sm-6">
			<div class="section__image animate animate__fade">
				<div class="image-border"></div>
				<img class="" alt="" src="<?php echo $image['url']; ?>" />
			</div>
		</div>

		<div class="col-sm-6">
			<div class="section__content ">
				<h2 class="animate animate__fade-up"><?php echo $heading; ?></h2>
				<p class="section__text animate animate__fade-up"><?php echo $text ?></p>
				<a class="button animate animate__fade-up" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>

				 <div class="cc-info inline animate animate__fade-up">
      
                        <div class="whour-content ">
                          <p class="cc-info__whour-title"><?php echo pll_e('Darbalaiks', 'Cash and carry') ?></p>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('Pr - C', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['mon-thur']; ?></p>
                          </div>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('Pk', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['friday']; ?></p>
                          </div>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('S', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['saturday']; ?></p>
                          </div>
                          <div class="cc-info__whour-info">
                              <p class="cc-info__whour-info__title"><?php echo pll_e('Sv', 'Cash and carry') ?></p>
                              <p class="cc-info__whour-info__info"><?php echo $hours['sunday']; ?></p>
                          </div>
                        </div>
 
                </div><!-- END cc-info -->

			</div>
		</div>

	</div>
</div>