@php
$slides = get_posts( array(
    'post_type' => 'slider',
    'numberposts' => -1,
    'post_status' => 'publish',
) );
@endphp
<div class="hero-wrapper">
	<div class="hero">
	@foreach ($slides as $slide)
		@php
		$image = get_the_post_thumbnail_url($slide->ID);

		$title = get_post_meta($slide->ID, 'slider_title');
		$text = get_post_meta($slide->ID, 'slider_text');
		$buttonText = get_post_meta($slide->ID, 'button_text');
		$buttonUrl = get_post_meta($slide->ID, 'button_url');
		$icons = rwmb_meta('hero_icon_image', array( 'size' => 'medium' ), $slide->ID);

		@endphp
		<div class="hero__item animate full-width">
			<div class="overlay"></div>
			<img class="hero__bg" data-lazy="{{ $image }}"/>
			<div class="container hero__content ">
	            <div class="hero__item__inner">
	            	<h2 class="hero__title">{{ $title[0] }}</h2>
	            		@if($text)
	                    	<div class="hero__text">{{ $text[0] }}</div>
	                    @endif
	                    @if($buttonUrl)
						<a href="{{ $buttonUrl[0] }}" class="button">{{ $buttonText[0] }}</a> 
						@endif
	           	</div>
	        </div>
	        <?php if( isset($icons) ) : ?>
				<?php foreach ($icons as $item) : ?>
	                <?php 
	                $icon = reset($item); 
	                ?>
	                <div class="hero__icons">
	                	<div class="container">
	                		<img src="{{ $icon['url'] }}" />
	                	</div>
	                </div>
	          	<?php endforeach; ?>
	      	<?php endif; ?>
		</div>
	@endforeach
	</div>
	<div class="slider-dots container mob-hidden "></div>
	
</div>
