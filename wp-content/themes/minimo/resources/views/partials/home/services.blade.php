<?php
$query = new WP_Query( array(
    'post_type' => 'pakalpojumi',
    'post_status' => 'publish',
    'numberposts' => 3,
    'post_parent' => 0,
) );
?>
<div class="section">
<div class="container">
	<div class="home-heading-content">
		<div class="home-heading-content__heading ">
			<h2 class="animate animate__fade"><?php echo pll_e('Pakalpojumi', 'General') ?></h2>
			
		</div>
		<div class="home-heading-content__button animate animate__fade mob-hidden">
			<a class="button--read-more animate animate__fade-up" href="/pakalpojumi"><?php echo pll_e('Skatīt visus pakalpojumus', 'General') ?> <span class="arrow"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span></a>
		</div>
	</div>
	<div class="row">
		@while($query->have_posts()) @php $query->the_post() @endphp
			<?php 
				$id = get_the_ID();
				$image = get_the_post_thumbnail_url($id, 'medium');
				$url = get_the_permalink();
				$short_desc = rwmb_meta('short_description');
				$desc_croped = tokenTruncate($short_desc, 150) . " ...";
			?>
			
			<div class="col-sm-4">
				<a href="{{ $url }}">
			
				<div class="posts__item animate animate__fade">
					<div class="posts__item__image animate animate__fade" style="background-image: url({{ $image }})">
					</div>
					<div class="posts__item__content animate animate__fade">
						<h3 class="posts__item__content__title"><?php echo get_the_title(); ?></h3>
						<p class="posts__item__content__text"><?php echo $desc_croped; ?></p>
						<a class="button--read-more animate" href="{{ $url }}"><?php echo pll__('Lasīt vairāk', 'General'); ?> <span class="arrow"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span></a>
					</div>
				</div>
				</a>
			</div>
			
		@endwhile
	</div>
</div>
</div>