<?php
$current_page = get_queried_object();
$id = $current_page->ID;
$heading = rwmb_meta('ind_heading', true, $id); 
$text = rwmb_meta('ind_text', true, $id); 
$items = rwmb_meta('ind_item', true, $id); 

?>
<div class="container section home-industry" >
	<div class="home-heading-content center">
		<div class="home-heading-content__heading animate animate__fade">
			<h2 class="animate animate__fade-up">{{ $heading }}</h2>
			
		</div>
		<div class="home-heading-content__text animate animate__fade">
			<p>{{ $text }}</p>
		</div>
	</div>
	<div class="home-industry__items">
		<div class="row">
			<?php foreach ($items as $item) : ?>
			<?php $image = wp_get_attachment_image_src($item['ind_image'][0], 'medium'); ?>
				<div class="col-sm-4">
					<div class="home-industry__items__item animate animate__fade">
						
						<img src="{{ $image[0] }}"/>
						<h3>{{ $item['ind_name'] }}</h3>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>