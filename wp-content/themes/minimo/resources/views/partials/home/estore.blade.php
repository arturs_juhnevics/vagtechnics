<?php 
	$heading = rwmb_meta('store_title');
	$text = rwmb_meta('store_text');
	$images = rwmb_meta( 'store_image', array( 'size' => 'large' ) );
	$image = reset($images);
	$estore_url = rwmb_meta( 'e_store_url', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
?>
<div class="section section--wide animate animate__fade" style="background-image: url(<?php echo $image['url']; ?>);">
	<div class="overlay overlay--solid"></div>
	<div class="border"></div>
	<div class="container">
		<div class="section--wide__content">
			<h3 class="animate animate__fade-up"><?php echo $heading; ?></h3>
			<p class="animate animate__fade-up"><?php echo $text; ?></p>
			<a class="button animate animate__fade-up" href="<?php echo $estore_url; ?>" target="_blank"><?php pll_e("E-Veikals", 'General'); ?></a>
		</div>
	</div>
</div>