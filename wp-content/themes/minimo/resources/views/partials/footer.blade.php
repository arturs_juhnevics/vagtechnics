<?php 
$lang = pll_current_language('slug'); 
$logos = rwmb_meta( 'logo', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$logo = reset( $logos );
$header_image = $logo['full_url'];


$address = rwmb_meta('cc_address', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$email = rwmb_meta('cc_email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$phone = rwmb_meta('cc_phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
$fax = rwmb_meta('cc_fax', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

$hours = rwmb_meta('working_hours', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

?>
<footer>
	<div class="container">
		<div class="footer-content clearfix animate animate__fade">
      <div class="row">
        <div class="footer-content__column">
          <p class="footer-content__title"><?php echo pll_e('Pakalpojumi', 'General') ?></p>
          <div class="menu animate">
          @if (has_nav_menu('footer_navigation'))
              {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']) !!}
            @endif
            
          </div>
        </div>

        <div class="footer-content__column animate animate__fade">
          <p class="footer-content__title"><?php echo pll_e('Sociālie tīkli', 'Contacts') ?></p>
          <?php
                  $platforms = array('twitter','youtube', 'facebook', 'linkedin', 'instagram', 'draugiem', 'google');
                  ?>
                   <ul class="socials">
                      <?php foreach ($platforms as $pl) : $_pl = rwmb_meta($pl, array( 'object_type' => 'setting',  'limit' => 1), 'settings');?>
                          <?php if(!empty($_pl)) : ?>
                              <li class="social">
                                  <a class="soc-item" target="_blank" href="<?php echo esc_url($_pl); ?>">
                                    <span class="social__icon"><?php echo file_get_contents(get_template_directory().'/assets/images/'.$pl.'.svg'); ?></span>
                                      <span class="social__text"><?php echo $pl; ?></span>                          
                                  </a>
                              </li>
                              <?php
                          endif;
                      endforeach; ?>
                  </ul>
        </div>
        <?php 
          $address = rwmb_meta('Address', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
          $email = rwmb_meta('email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
          $phone = rwmb_meta('phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
        ?>
        <div class="footer-content__column animate animate__fade">
          <p class="footer-content__title"><?php echo pll_e('Kontakti', 'Contacts') ?></p>
          <div class="row">
              <div class="col-sm-6">
                <p class="footer-content__text">{{ $address }}</p>
                <a class="button--read-more" href="/kontakti"><?php echo pll_e('Kontakti', 'Contacts') ?> <span class="arrow"><?php echo file_get_contents(get_template_directory_uri()."/assets/images/arrow-right.svg"); ?></span></a>
              </div>
              <div class="col-sm-6">
                <a class="email" href="mailto:{{ $email }}">{{ $email }}</a>
                <a class="phone" href="tel:{{ $phone }}">{{ $phone }}</a>
              </div>              
          </div>
        </div>

      </div><!-- END row -->
			

		</div><!-- END footer-content -->
	</div>
</footer>

