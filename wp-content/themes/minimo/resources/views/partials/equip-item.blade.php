<?php 
	$id = get_the_ID();
	$image = get_the_post_thumbnail_url($id, 'large');
	$spec = rwmb_meta('specification', false, $id);
	$sub_title = rwmb_meta('sub_title', false, $id);
	$title = get_the_title();
?>
<div class="equipment-item clearfix">
	<div class="equipment-item__image animate animate__fade">
		<img alt="" src="{{ $image }}" />
	</div>
	<div class="equipment-item__content animate animate__fade">
		<h2>{{ $title }}</h2>
		<p class="sub-title">{{ $sub_title }}</p>
		<div class="group-wrapper">
			<?php foreach ($spec as $item) : ?>
				<div class="equipment-item__group">
					<p class="equipment-item__group__title">{{ $item['eq_title'] }}</p>
					<p class="equipment-item__group__value">{{ $item['eq_value'] }}</p>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>