{{--
  Page for Pakalpojumi
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')

<div class="container clearfix">
	@include('partials.side-menu-main')
	<div class="main-content">
		<div class="service-content clearfix">
			<div class="entry-content animate animate__fade">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</div>
</div>

@endsection