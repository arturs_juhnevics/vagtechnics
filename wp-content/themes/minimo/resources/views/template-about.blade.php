{{--
  Template Name: About us
--}}
<?php 
$icons = rwmb_meta('about_icon_image', array( 'size' => 'medium' ));
?>
@extends('layouts.app')

@section('content')
@include('layouts.page-header')
  @while(have_posts()) @php the_post() @endphp
  <div class="container">
  		<div class="row">
		  	<div class="col-sm-9">
		  		 <div class="entry-content about-page">
					@php the_content() @endphp
				</div>
		  	</div>
		   <div class="col-sm-3">
		   		<div class="about-side-menu">
		   			<?php if( isset($icons) ) : ?>
						<?php foreach ($icons as $item) : ?>
			                <?php 
			                $icon = reset($item); 
			                ?>
			                <div class="about-page__icons">
			                	<img src="{{ $icon['url'] }}" />
			                </div>
			          	<?php endforeach; ?>
			      	<?php endif; ?>
		   		</div>
		   </div>
	   </div>
   </div>
  @endwhile
@endsection