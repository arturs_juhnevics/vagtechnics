  <!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <?php 
  $quer_object = get_queried_object();
  $bgImage = "";
  if(isset($quer_object->name) && $quer_object->name == 'pakalpojumi'){
    $bgImage = "bg-image";
  }
  
  ?>
  <body @php body_class( $bgImage ) @endphp>
    @php do_action('get_header') @endphp
    <?php if( !isset($_COOKIE['set_lang'])){ ?>
        @include('partials.language')
    <?php } ?>
    
    @include('partials.header')
      <div class="wrapper">

        <div class="main">
          @yield('content')
          @include('partials.footer')
        </main>
      </div>
    @php do_action('get_footer') @endphp
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/rellax.min.js"></script>
     <script async data-main="<?php echo get_template_directory_uri(); ?>/assets/js/app.js?v=1.1.1"
                src="<?php echo get_template_directory_uri(); ?>/assets/js/require.min.js"></script>
    @php wp_footer() @endphp
    @include('partials.photoswipe')
</html>
