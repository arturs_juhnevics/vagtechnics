{{--
  Template Name: Contacts
--}}
@extends('layouts.app')
@section('content')
@include('layouts.page-header')
<?php 
  $address = rwmb_meta('Address', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $email = rwmb_meta('email', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $phone = rwmb_meta('phone', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');
  $time = rwmb_meta('opening_times', array( 'object_type' => 'setting',  'limit' => 1 ), 'settings');

  $actual_address = rwmb_meta('actual_address'); 
  $legal_address = rwmb_meta('legal_address');  
  $pvn = rwmb_meta('pvn_no');
  $lum_bank = rwmb_meta('lum_bank');
  $lum_bank_code = rwmb_meta('lum_bank_code');
  $swed_bank = rwmb_meta('swed_bank');
  $swed_bank_code = rwmb_meta('swed_bank_code');

  $contact_text = rwmb_meta('contact_text');

  $pin_icon = file_get_contents(get_template_directory_uri()."/assets/images/pin.svg");
  $email_icon = file_get_contents(get_template_directory_uri()."/assets/images/email.svg");
  $phone_icon = file_get_contents(get_template_directory_uri()."/assets/images/phone.svg");
  $time_icon = file_get_contents(get_template_directory_uri()."/assets/images/time.svg");

  $enable_team = rwmb_meta('enable_team'); 
?>
  @while(have_posts()) @php the_post() @endphp

    <div class="contacts contacts--sec container">

      <div class="row">
        
        <div class="col-sm-6">
          <h2 class="contacts__title animate animate__fade"><?php echo pll_e('Kontaktinformācija', 'Contacts') ?></h2>
          <div class="general-info">

            <div class="row">
              <div class="col-sm-6">
                <p class="general-info__title animate animate__fade"><span><?php echo $pin_icon; ?></span><?php echo pll_e('Biroja adrese', 'Contacts') ?></p>
                <p class="general-info__text top animate animate__fade"><?php echo $address; ?></p>
              </div>
              <div class="col-sm-6">
                <p class="general-info__title animate animate__fade"><span><?php echo $phone_icon; ?></span><?php echo pll_e('Telefons', 'Contacts') ?></p>
                <a class="general-info__text top animate animate__fade" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <p class="general-info__title animate animate__fade"><span><?php echo $time_icon; ?></span><?php echo pll_e('Darbalaiks', 'Contacts') ?></p>
                <p class="general-info__text animate animate__fade"><?php echo $time; ?></p>
              </div>
              <div class="col-sm-6">
                <p class="general-info__title animate animate__fade"><span><?php echo $email_icon; ?></span><?php echo pll_e('E-pasts', 'Contacts') ?></p>
                <a class="general-info__text animate animate__fade"href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
              </div>
            </div>

          </div><!-- END .genereal-info -->
        </div><!-- END .col-sm-6 -->

        <div class="col-sm-6">
          <div class="company-details">
            <h2 class="contacts__title animate animate__fade rekviziti"><?php echo pll_e('Rekvizīti', 'Contacts') ?></h2>
            <div class="row">
                <div class="col-sm-6">
                  <div class="company-details__group animate animate__fade">
                    <p class="company-details__title"><?php echo pll_e('PVN reģistrācijas numurs', 'Contacts') ?></p>
                    <p class="company-details__info"><?php echo $pvn; ?></p>
                  </div>
                  <div class="company-details__group animate animate__fade">
                    <p class="company-details__title"><?php echo pll_e('Juridiskā adrese', 'Contacts') ?></p>
                    <p class="company-details__info"><?php echo $legal_address; ?></p>
                  </div>
                  <div class="company-details__group animate animate__fade">
                    <p class="company-details__title"><?php echo pll_e('Faktiskā adrese', 'Contacts') ?></p>
                    <p class="company-details__info"><?php echo $actual_address; ?></p>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="company-details__group animate animate__fade">
                    <p class="company-details__title"><?php echo pll_e('Luminor bankas konts', 'Contacts') ?></p>
                    <p class="company-details__info"><?php echo $lum_bank; ?></p>
                    <p class="company-details__info"><?php echo $lum_bank_code; ?></p>
                  </div>
                  <div class="company-details__group animate animate__fade">
                    <p class="company-details__title"><?php echo pll_e('Swedbankas konts', 'Contacts') ?></p>
                    <p class="company-details__info"><?php echo $swed_bank; ?></p>
                    <p class="company-details__info"><?php echo $swed_bank_code; ?></p>
                  </div>
                </div>

            </div>
          </div><!-- END .company details -->
        </div><!-- END .col-sm-6 -->

      </div>
    </div><!-- END .container contacts -->
    <?php if( $enable_team ) : ?>
    <div class="contacts contacts--sec2 container">
        <?php 
          $team = rwmb_meta('team_members'); 
        ?>
        <h2 class="contacts__title animate animate__fade"><?php echo pll_e('Komanda', 'Contacts') ?></h2>
        <div class="row">
            <?php foreach ($team as $item) : ?>
              <div class="col-sm-3">
                <div class="team animate animate__fade">
                  <p class="team__item position">{{ $item['position'] }}</p>
                  <p class="team__item">{{ $item['name'] }}</p>
                  <?php foreach($item['phone'] as $single) : ?>
                    <a class="team__item" href='tel:{{ $single }}'>{{ $single }}</a>
                  <?php endforeach; ?>
                  <a class="team__item" href="mailto:{{ $item['email'] }}">{{ $item['email'] }}</a>
                </div>
              </div>
            <?php endforeach; ?>
        </div>
    </div><!-- END .container contacts--sec2 -->
    <?php endif; ?>
    <div class="contacts contacts--sec3">
      <div class="full-width-block">

          <div class="container">
            <h2 class="contacts__title animate animate__fade"><?php echo pll_e('Sazinies ar mums', 'Contacts') ?></h2>
            <div class="contacts__text animate animate__fade"><?php echo $contact_text; ?></div>
            <div class="row">
              <div class="col-sm-6">
                <div class="contacts__form animate animate__fade">
                  <form id="contact-form" method="POST">
                      <div class="row">
                          <div class="col-sm-6 animate animate__fade-up">
                              <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Vārds', 'Contact-form'); ?><span class="req">*</span></span>
                                  <input type="text" name="name" id="name" class="required"/>
                              </label>
                          </div>
                          <div class="col-sm-6 animate animate__fade-up">
                             <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Telefons', 'Contact-form'); ?><span class="req">*</span></span>
                                  <input type="text" name="phone" id="phone" class="required"/>
                              </label>
                          </div>
                          <div class="col-sm-12 animate animate__fade-up">
                              <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('E-pasts', 'Contact-form'); ?><span class="req">*</span></span>
                                  <input type="text" name="email" id="email" class="required"/>
                              </label>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12 animate animate__fade-up">
                             <label class="input__block">
                                  <span class="input__block__label"><?php echo pll__('Ziņa', 'Contact-form'); ?><span class="req">*</span></span>
                                  <textarea name="message" id="message" > </textarea>
                              </label>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12 animate animate__fade-up">
                             <div class="dropzone" id="dZUpload">
                                <div class="dz-default dz-message">
                                   <button type="button" class="dz-button"><?php echo pll__('Ievelc failus šeit vai spied lai augšuplādētu', 'Contact-form'); ?></button><br>
                                </div>
                             </div>
                          </div>
                      </div>
                          <div id="recaptcha_element"></div>
                          <div class="button-container">
                              <div class="form__status hidden"></div>
                              <button class="button" class="contacts__form__submit"><?php echo pll__('Nosūtīt', 'Contact-form'); ?></button>
                          </div>
                          <div class="col-sm-12">
                              <?php wp_nonce_field('contact-nonce', 'contact-nonce'); ?>
                              <?php $from = '';
                              if (isset($_GET['from'])) {
                                  $from = strip_tags($_GET['from']);
                              } ?>
                              <input type="hidden" name="page_from" id="page_from" value="<?php echo $from; ?>" class="leave"/>
                              <input type="hidden" name="post_id" id="post_id" value="<?php echo $post->ID; ?>" class="leave"/>
                              <input type="hidden" name="action" id="action" value="contact_submit" class="leave"/>
                              <input type="text" name="contact-honey" id="contact-honey" class="hidden"/>
                              
                          </div>
                        </form>
                    </div>
              </div>
              <div class="col-sm-6"></div>
            </div>
          </div>

          <div class="full-width-block__left"></div>
          <div class="full-width-block__right">
            <?php
              $map = rwmb_get_value( 'contact_map');
              $pins = rwmb_meta('contact_map_pin');
              $pin = reset( $pins );

              ?>
              <?php if (!empty($map)) : ?>
                  <div class="contacts__map animate animate__fade">
                    <div id="map-wrap"
                           class="map-wrap"
                           data-lng="<?php echo $map['longitude'] ?>"
                           data-lat="<?php echo $map['latitude'] ?>"
                           data-zoom="<?php echo $map['zoom']; ?>"
                           data-pin="<?php echo $pin['url']; ?>">
                             
                    </div>
                  </div>
              <?php endif; ?>
          </div>
      </div>
    </div><!-- END .container contacts--sec3 -->


    <div class="contacts container">
        
        <div class="col-sm-6 contacts-column animate animate__fade-up">
          

        </div>   

      </div>

    </div>

  @endwhile
  
@endsection


   			