var PhotoSwipe;
var PhotoSwipeUI_Default;

var photoswipe = {

    /*
     * lookbook post url
     */
    post_url: '',

    /*
     * gallery title
    */
    post_title: '',

    /*
     * owlCarousel thumbnails slider object
     */
    slick_thumbs: false,

    /*
     * photoswipe gallery object
     */
    gallery: false,

    /*
     * lookbook images array
     */
    images: [],

    /*
     * index of the current image
     */
    current: 0,

    show_soc : true,

    downloadable : false,

    /*
     * options for owlCarousel thumbnails slider
     */
    slick_options: {
        slidesToShow: 15,
        dots: false,
        arrows: false,
        afterInit: function(el) {
            el.find(".owl-item").eq(photoswipe.current).addClass("synced");
        },
        afterMove: function() {

            var number = this.currentItem;
            photoswipe.slick_thumbs
                .find(".owl-item")
                .removeClass("synced")
                .eq(number)
                .addClass("synced");
            if (photoswipe.slick_thumbs.data("owlCarousel") !== undefined) {
                var sync2visible = photoswipe.slick_thumbs.data("owlCarousel").owl.visibleItems;

                var found = false;
                for (var i in sync2visible) {
                    if (number === sync2visible[i]) {
                        var found = true;
                    }
                }
            }
        }
    },
    
    init : function( PS, UI, items, num, galID, url, title ){

        PhotoSwipe = PS;
        PhotoSwipeUI_Default = UI;

        photoswipe.images = items;
        photoswipe.post_url = url;
        photoswipe.post_title = title;

        if( url === false ) {
            photoswipe.show_soc = false;
        }
        
        photoswipe.setup_data( items, num, galID );

        var pswpElement = document.querySelectorAll('.pswp')[0],
            options = {
                preload       : [1, 1],
                closeOnScroll : false,
                index         : 0,
                galleryUID    : galID,
                fullscreenEl  : false,
                zoomEl        : false,
                shareEl       : false,
                counterEl     : false,
                clickToCloseNonZoomable: false,
                closeElClasses: ['caption', 'zoom-wrap', 'ui'],
                allowUserZoom: false,
                maxSpreadZoom: 1,
                getDoubleTapZoom: function (isMouseClick, item) {
                    return item.initialZoomLevel;
                },
                history: false,
                tapToToggleControls : false,
                barsSize: {
                    top: 61,
                    bottom: 61,
                }
            };
        if ($('body').is('.single-lookbook')) {
            options.escKey = false;
        }
        if( photoswipe.show_soc === false ) {
            $('.share-wrap.lookbook-share').css({
                display : 'none'
            });
        }
        if( photoswipe.downloadable ) {
            $('.image-download-btn').css({
                display: 'inline-block'
            });
            $( '.image-download-btn' ).attr('href', this.images[ this.current ].src);
        }

        photoswipe.gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        photoswipe.gallery.init();

        $('body').on('click touchend', '.pin', function(e) {
            e.preventDefault();
            var dwrap = $('.desc-wrap'),
                pindex = $(this).attr('data-index');

            if( typeof( items[photoswipe.current].pins[pindex] ) == 'object' ) {
                var pdata = items[photoswipe.current].pins[pindex];
                dwrap.find('h2').text( photoswipe.post_title );
                dwrap.find('h4').text( pdata.pin_title );
                dwrap.find('.entry-content').html( '<p>' + pdata.pin_description + '</p>' );
                $('.desc-wrap').addClass('visible');    
            }
            
        }).on('click touchend', '.desc-close', function(e) {
            e.preventDefault();
            $('.desc-wrap').removeClass('visible');
        });

        $('.pswp__button--close').on('click touchend', function(e) {
            e.preventDefault();
            $('.pswp').removeClass('pswp--open');
        });

        if (typeof(num) != 'undefined') {
            photoswipe.gallery.goTo(num);
            $('.gal-info .current').text(num + 1);
            photoswipe.current = num;
            $('.gal-info .current').text(num + 1);
        }

        photoswipe.gallery.listen('afterChange', function() {

            var index = photoswipe.gallery.getCurrentIndex();
            photoswipe.current = index;

            $('.overlay-thumbs').find('.pswp-slide.synced').removeClass('synced');
            $('.overlay-thumbs').find('.pswp-slide').eq(index).addClass('synced');
            $('.gal-info .current').text(index + 1);

            $('.desc-wrap').removeClass('visible');
        });


        photoswipe.gallery.listen('beforeResize', function(){
            if($(window).width() > 995) photoswipe.gallery.viewportSize.x = photoswipe.gallery.viewportSize.x - 120;
            else photoswipe.gallery.viewportSize.x = photoswipe.gallery.viewportSize.x;
        });
        window.dispatchEvent(new Event('resize'));

        photoswipe.gallery.listen('destroy', function() {
            $('.desc-wrap').removeClass('visible');
        });

    },

    setup_data: function(images, post_id, img_index) {

        $('.pswp__button').removeClass('inactive');
        $('.pswp').attr('data-post-id', post_id);
        $('.pswp-gallery-title').text(photoswipe.post_title);

        if (photoswipe.slick_thumbs != false) {
            $('.overlay-thumbs').slick('unslick');
        }
        $('.overlay-thumbs').html('');

        if (images.length < 2) {
            var display = 'none';
        } else {
            var display = 'block';
            if (!$('.overlay-thumbs').children('div').length) {

                for (var i = 0; i < images.length; i++) {
                    $('.overlay-thumbs').append('<div class="pswp-slide" data-index="' + i + '"><div class="inner"><div class="thumb" style="background-image:url('+images[i].thumb+')"></div></div></div>');
                }
                if (i < 2) {
                    var display = 'none';
                }
            }

        }

        $('.overlay-thumbs-outer').css('display', display);
        $('.gal-info .total').text(i);
    }
};