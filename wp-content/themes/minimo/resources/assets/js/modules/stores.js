var smartmap = {
    data : locations,
    city : '0all',
    cityAll: 'Visas pilsētas',
    filtered: [],
    cities: [],
    active: false,
    markers: [],
    userLocation: false,

    init : function () {
        smartmap.filters();
        smartmap.popup();
        if( $('#map-wrap').length ) {
            require(['https://cdn.jsdelivr.net/npm/gmaps-marker-clusterer@1.2.2/src/markerclusterer.js', 'async!https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,drawing&key='+gmap_api_key], function() {
                require(['https://cdn.jsdelivr.net/npm/google-maps-utility-library-v3-markerwithlabel@1.1.10/dist/markerwithlabel.min.js'], function() {
                    smartmap.maps.init( $('#map-wrap') );
                });
            });
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                smartmap.userLocation = position.coords;
            });
        }
        
    },

    popup : function () {
        $('.smartmap__seller').each(function(){
            var self = $(this);
            $('.smartmap__seller__close').click(function(){
                self.removeClass('active');
            });
            
        });
    },

    setFiltersOptions: function (updated, preventMapUpdate) {
        smartmap.cities = [{
            value: '0all',
            text: smartmap.cityAll,
            order: 0,
        }];
      
        smartmap.filtered = smartmap.data.filter(function(item){

                var cities = smartmap.cities.find(function(o){return o.text === item.city;});
                if(!cities) smartmap.cities.push({
                    value: item.city,
                    text: item.city,
                });
       
            if(smartmap.markers[item.id]) smartmap.markers[item.id].setVisible(false);
            if((item.city == smartmap.city || smartmap.city == '0all')){
                if(smartmap.markers[item.id]) smartmap.markers[item.id].setVisible(true);
                return item;
            }
        });
        if(!updated || updated == 'city') $('.smartmap__filter--city select').each(function(){
            var selectize = $(this)[0].selectize;
            selectize.clearOptions();
            smartmap.cities.forEach(function(city){
                selectize.addOption(city);
            });
        });
        if(smartmap.maps.map && !preventMapUpdate) smartmap.maps.update();
    },

    filters: function () {
        $('.smartmap__filter').each(function(){
            var that = $(this);
            var $select = $('select', that).selectize({
                create: false,
                sortField: [{field: 'value', direction: 'asc'}, {field: '$score'}],
                render: {
                    item: function(item, escape) {
                        $('.selectize-input smartmap__filter__prefix', that).remove();
                        return '<div>'
                        + $('.smartmap__filter__prefix', that)[0].outerHTML
                        + '<span class="smartmap__filter__value">'+item.text+'</span> '
                        + '</div>';
                    }
                },
                onChange: function(value){
                    if(that.hasClass('smartmap__filter--city')){
                        smartmap.city = value;
                    } else {
                        smartmap.seller = value;
                    }
                    $('.smartmap__popup, .smartmap__seller').removeClass('active');
                    smartmap.setFiltersOptions(that.hasClass('smartmap__filter--city') ? 'seller' : 'city');
                },
            });
            $(this).addClass('ready');
            var selectize = $select[0].selectize;
            $( '.selectize-control', that ).on( 'click', function () {
                selectize.clear(true);
                if(!$('.selectize-input .smartmap__filter__prefix', that).length) $('.selectize-input', that).prepend($($('.smartmap__filter__prefix', that)[0].outerHTML));
            });
            selectize.on('blur', function() {
                if (selectize.getValue() === '') {
                    var defaultOption = selectize.options[Object.keys(selectize.options)[0]];
                    selectize.setValue(defaultOption.value);
                }
            });
        });
        smartmap.setFiltersOptions();
    },

    maps: {

        elem: false,
        map: false,
        pinIcon: '',
        cluserIcon: '',

        init: function (elem) {
            smartmap.matrix = new google.maps.DistanceMatrixService();

            this.elem = elem;
            var self = this;
            var mapDiv = this.elem[0];

            if(self.elem.data('pin')) self.pinIcon = self.elem.data('pin');
            if(self.elem.data('cluster')) self.cluserIcon = self.elem.data('cluster');

            var map = new google.maps.Map(mapDiv, {
                disableDefaultUI: false,
                scrollwheel: false,
                draggable: true,
                mapTypeControl: false,
                fullscreenControl: false,
                maxZoom: 18,
                styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
            });
            smartmap.maps.map = map;

            var markers = [];
            smartmap.data.forEach(function(location){
                var marker = new MarkerWithLabel({
                    position: new google.maps.LatLng(location.lat, location.lng),
                    optimized: false,
                    icon: self.pinIcon,
                    labelContent: '',
                    labelAnchor: new google.maps.Point(12, 30),
                    labelClass: "smartmap__pin pulse animated infinite", // your desired CSS class
                    
                });
                marker.metadata = {id: location.id};
                marker.addListener('click', function () {
                    $('.smartmap__popup, .smartmap__seller').removeClass('active');
                    smartmap.maps.openMarker(location.id);
                    map.setCenter(marker.getPosition());
                    map.setZoom(15);
                });
                 $('.view-on-map').click( function(e){
                  
                    var id = $(this).attr('data-id');
                    if(id == marker.metadata.id){
                        
                        map.setCenter(marker.getPosition());
                        map.setZoom(15);
                        smartmap.maps.openMarker(id);

                    }
                   
                });
                
                if(!markers[location.city]) markers[location.city] = [];
                markers[location.city].push(marker);
                smartmap.markers[location.id] = marker;
            });


            Object.keys(markers).forEach(function(key){
                var markerCluster = new MarkerClusterer(map, markers[key], {
                    cssClass: 'cluster-icon',
                    ignoreHiddenMarkers: true,
                });
                google.maps.event.addListener(markerCluster, 'clusterclick', function(cluster) {
                    $('.smartmap__popup, .smartmap__seller').removeClass('active');
                    if(smartmap.city != key){
                        smartmap.city = key;
                        $('.smartmap__filter--city select').each(function(){
                            $(this)[0].selectize.setValue(key);
                        });
                        smartmap.setFiltersOptions('seller', true);
                    }
                });
            });

            smartmap.maps.update();

            google.maps.event.addListenerOnce(this.map, "projection_changed", function(){
                smartmap.maps.update();
            });
        },

        openMarker: function (id) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            console.log('hello2')
            smartmap.active = id;
            var seller = $('.smartmap__seller');
            var data = smartmap.data.filter(function(item){
                if(item.id === id) return item;
            })[0];
            var userLatLng = smartmap.userLocation ? smartmap.userLocation.latitude + ','+smartmap.userLocation.longitude : ''
            $('.smartmap__seller__logo img').attr('src', data.logo);
            $('.smartmap__seller__title').text(data.name);
            $('.smartmap__seller__distance .number').text('').addClass('loading');
            $('.smartmap__seller__directions a').attr('href', "https://www.google.lv/maps/dir/"+userLatLng+"/"+data.lat+","+data.lng+"/");
            $('.smartmap__seller__address').text(data.address);
            $('.smartmap__seller__hours').text(data.hours);
             $('.smartmap__seller__name').text(data.seller);
            $('.smartmap__seller__info--phone').text(data.phone);
            $('.smartmap__seller__info--mail a').text(data.mail).attr('href', 'mailto:'+ data.mail);
            $('.smartmap__seller__info--web a').text(data.web).attr('href', data.web);
            seller.addClass('active');
            if(!smartmap.userLocation)$('.smartmap__seller__distance').remove();
            else{
                smartmap.matrix.getDistanceMatrix(
                {
                    origins: [{lat: parseFloat(smartmap.userLocation.latitude), lng: parseFloat(smartmap.userLocation.longitude)}],
                    destinations: [{lat: parseFloat(data.lat), lng: parseFloat(data.lng)}],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                }, function(response, status){
                    if(response.rows.length){
                        $('.smartmap__seller__distance .number').removeClass('loading').text(
                            response.rows[0].elements[0].distance.text
                        );
                    }
                });
            }
        },

        update:  function () {
            $(smartmap.maps.elem).removeClass('loaded');
            var bounds = new google.maps.LatLngBounds();
            smartmap.filtered.forEach(function(item){
                bounds.extend(new google.maps.LatLng(item.lat, item.lng));
            });

            if (smartmap.maps.map.getProjection()) {
                var offsety = $('.page-header').outerHeight() + $('.smartmap__filters').height() + 30;
                
                var point1 = smartmap.maps.map.getProjection().fromLatLngToPoint(bounds.getNorthEast());
                smartmap.maps.map.fitBounds(bounds);
                
                var point2 = new google.maps.Point(
                    ( (typeof(offsetx) == 'number' ? offsetx : 0) / Math.pow(2, smartmap.maps.map.getZoom()) ) || 0,
                    ( (typeof(offsety) == 'number' ? offsety : 0) / Math.pow(2, smartmap.maps.map.getZoom()) ) || 0
                );          
                
                var newPoint = smartmap.maps.map.getProjection().fromPointToLatLng(new google.maps.Point(
                    point1.x - point2.x,
                    point1.y - point2.y
                ));
               
                bounds.extend(newPoint);
                smartmap.maps.map.fitBounds(bounds);
                setTimeout(function(){
                    $(smartmap.maps.elem).addClass('loaded');
                }, 500);
            }
        },

    },
}
smartmap.init();