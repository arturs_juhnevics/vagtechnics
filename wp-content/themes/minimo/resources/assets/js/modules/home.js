var home = {
	init: function (){

		$('.hero').not('.slick-initialized').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          fade: true,
          autoplay: true,
          arrows: false,
          autoplaySpeed: 5000,
          adaptiveHeight: false,
          draggable: true,
          dots:true,
          lazyLoad: 'ondemand',
          appendDots: $('.slider-dots'),
       });


     $('.posts').not('.slick-initialized').slick({
         slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          fade: false,
          autoplay: false,
          arrows: true,
          autoplaySpeed: 10000,
          adaptiveHeight: true,
          draggable: true,
  
          prevArrow: $('.arrow-left'),
          nextArrow: $('.arrow-right'),
          responsive: [
              {
                  breakpoint: 890,
                  settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '30px',
                  }
              },
              {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '30px',
                  }
              }
          ]
       });
	}
}
home.init();