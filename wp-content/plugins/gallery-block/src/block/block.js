(function (blocks, editor, components, i18n, element) {
  var __ = i18n.__
  var el = element.createElement
  var registerBlockType = blocks.registerBlockType
  var RichText = editor.RichText
  var BlockControls = editor.BlockControls
  var AlignmentToolbar = editor.AlignmentToolbar
  var MediaUpload = editor.MediaUpload
  var InspectorControls = editor.InspectorControls
  var PanelBody = components.PanelBody
  var TextControl = components.TextControl

  registerBlockType('cgb/block-gallery-block', { // The name of our block. Must be a string with prefix. Example: my-plugin/my-custom-block.
    title: __('Profile'), // The title of our block.
    description: __('A custom block for displaying personal profiles.'), // The description of our block.
    icon: 'businessman', // Dashicon icon for our block. Custom icons can be added using inline SVGs.
    category: 'common', // The category of the block.
    supports: {
      align: true,
      alignWide: true
    },
    attributes: { // Necessary for saving block content.
      mediaID: {
        type: 'number'
      },
      mediaURL: {
        type: 'string',
        source: 'attribute',
        selector: 'img',
        attribute: 'src'
      },
      alignment: {
        type: 'string',
        default: 'center'
      },
     
    },

    edit: function (props) {
      var attributes = props.attributes
      var alignment = props.attributes.alignment

      var onSelectImage = function (media) {
        return props.setAttributes({
          mediaURL: media.url,
          mediaID: media.id
        })
      }

      function onChangeAlignment (newAlignment) {
        props.setAttributes({ alignment: newAlignment })
      }

      return [


		el(MediaUpload, {
            onSelect: onSelectImage,
            type: 'image',
            value: attributes.mediaID,
            gallery: true,
            addToGallery: true,
            multiple: true,
            multiple: true,
            render: function (obj) {
              return el(components.Button, {
                className: attributes.mediaID ? 'image-button' : 'button button-large',
                onClick: obj.open
              },
              !attributes.mediaID ? __('Upload Images') : el('img', { src: attributes.mediaURL })
              )
            }
          })
        
      ]
    },

    save: function (props) {
      var attributes = props.attributes
      var alignment = props.attributes.alignment
      var imageClass = 'wp-image-' + props.attributes.mediaID

      return (
        el('div', { className: props.className },
          attributes.mediaURL && el('div', { className: 'organic-profile-image', style: { backgroundImage: 'url(' + attributes.mediaURL + ')' } },
            el('figure', { class: imageClass },
              el('img', { src: attributes.mediaURL, alt: __('Profile Image') })
            )
          ),
          el('div', { className: 'organic-profile-content', style: { textAlign: alignment } },
            attributes.title && el(RichText.Content, {
              tagName: 'h3',
              value: attributes.title
            }),
            attributes.subtitle && el(RichText.Content, {
              tagName: 'h5',
              value: attributes.subtitle
            }),
            el(RichText.Content, {
              className: 'organic-profile-bio',
              tagName: 'p',
              value: attributes.bio
            }),
            el('div', { className: 'organic-profile-social' },
              attributes.facebookURL && el('a', {
                className: 'social-link facebook-link',
                href: attributes.facebookURL,
                target: '_blank',
                rel: 'noopener noreferrer'
              },
              el('i', { className: 'fa fa-facebook' })
              ),
              attributes.twitterURL && el('a', {
                className: 'social-link twitter-link',
                href: attributes.twitterURL,
                target: '_blank',
                rel: 'noopener noreferrer'
              },
              el('i', { className: 'fa fa-twitter' })
              ),
              attributes.instagramURL && el('a', {
                className: 'social-link instagram-link',
                href: attributes.instagramURL,
                target: '_blank',
                rel: 'noopener noreferrer'
              },
              el('i', { className: 'fa fa-instagram' })
              ),
              attributes.linkedURL && el('a', {
                className: 'social-link linkedin-link',
                href: attributes.linkedURL,
                target: '_blank',
                rel: 'noopener noreferrer'
              },
              el('i', { className: 'fa fa-linkedin' })
              ),
              attributes.emailAddress && el('a', {
                className: 'social-link email-link',
                href: 'mailto:' + attributes.emailAddress,
                target: '_blank',
                rel: 'noopener noreferrer'
              },
              el('i', { className: 'fa fa-envelope' })
              )
            )
          )
        )
      )
    }
  })
})(
  window.wp.blocks,
  window.wp.editor,
  window.wp.components,
  window.wp.i18n,
  window.wp.element
)